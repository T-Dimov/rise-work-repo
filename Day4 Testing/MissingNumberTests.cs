using Day4_Matrix_Algorithms;
namespace Day4_Testing
{
    [TestClass]
    public class MissingNumberTests
    {
        [TestMethod]
        public void TestMissing1()
        {
            //Arrange
            int[] array = { 3, 1, 4};
            //Act
            int missing_num = MissingNumber.FindMissing(array);
            //Assert
            Assert.AreEqual(2, missing_num);
        }

        [TestMethod]
        public void TestMissing2()
        {
            //Arrange
            int[] array = { 4, 2, 5, 7, 3};
            //Act
            int missing_num = MissingNumber.FindMissing(array);
            //Assert
            Assert.AreEqual(6, missing_num);
        }

        [TestMethod]
        public void TestMissing3()
        {
            //Arrange
            int[] array = { 103, 101 };
            //Act
            int missing_num = MissingNumber.FindMissing(array);
            //Assert
            Assert.AreEqual(102, missing_num);
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "No missing number")]
        public void TestMissingNoMissing()
        {
            //Arrange
            int[] array = { 103, 101, 102 };
            //Act
            int missing_num = MissingNumber.FindMissing(array);
            //Assert
            //Assert.Fail();
        }
    }
}